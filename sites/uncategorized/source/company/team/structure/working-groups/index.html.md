---
layout: markdown_page
title: "Working Groups"
description: "Like all groups at GitLab, a working group is an arrangement of people from different functions. Learn more!"
canonical_path: "/company/team/structure/working-groups/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What's a Working Group?

Like all groups at GitLab, a [working group](https://en.wikipedia.org/wiki/Working_group) is an arrangement of people from different functions. What makes a working group unique is that it has defined roles and responsibilities, and is tasked with achieving a high-impact business goal. A working group disbands when the goal is achieved (defined by exit criteria) so that GitLab doesn't accrue bureaucracy.

### CEO Handbook Learning Discussion on Working Groups

GitLab's CEO, Sid, and Chief of Staff to the CEO, Stella, and the Learning & Development team discuss Working Groups in detail during a [CEO handbook learning session](/handbook/people-group/learning-and-development/learning-initiatives/#ceo-handbook-learning-sessions).

Topics covered include:
1. What is a working group
1. When to start a working group
1. Difference between project managers and working groups.
   - Note: GitLab does not internally have project managers. Individual team members should have agency and accountability. We don't want someone who just tracks the status of work and makes updates. We want people to do this directly, and we want the [DRI](/handbook/people-group/directly-responsible-individuals/) to own the process and outcome. This supports accountability. We sometimes have project managers when interacting with external organizations, because accountability is harder when working with external parties.
1. Lessons learned from previous working groups
1. Why and how working groups support cross-functional projects

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/tE3d8WUSL30" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Roles and Responsibilities

### Required Roles

**Working Group Directly Responsible Individual**

This is the **person ultimately responsible for the success of the Working Group**. Assembles the working group, identifies interdependencies, manages supporting individuals toward project success, and ensures that results and escalations are appropriately communicated. This person also plays the facilitator role. A facilitator is responsible for running meetings and supporting the operational efficiency and success of a working group.

1. Makes sure that team objectives and success measurement is known and understood by team members and key stakeholders.
1. Identifies interdependencies and risks to then ensure that team members are proactively aligning with others and prioritizing necessary risk mitigations.
1. Clearly manages different project workstreams and proactively communicates expectations for specific working group team members. For example, if you know that an initiative will eventually involve customer communications, the Working Group DRI should ensure that the group has appropriate representation from the Customer Success Team and that the representative is clear on asks for both the team and the individual within the coming weeks and quarters.
1. Assigns any actions, initiatives, or outstanding questions to a workstream [DRI](/handbook/people-group/directly-responsible-individuals/) to investigate further. This ensures accountability and prevents overwhelming any single member or the Working Group DRI.
1. Evaluates opportunities to work in a way that better reflects GitLab values. Examples:
   1. Suggests more [efficient](https://about.gitlab.com/handbook/values/#efficiency) ways to get to the same outcome
   1. Considers how decisions or milestones can be unbundled, so we can work more [iteratively](https://about.gitlab.com/handbook/values/#iteration)
1. Escalates when scope is changing, the project is at risk, or greatership engagement is required.
1. Owns meeting agendas, so that they are organized to stay on topic. Follows up on goals from the previous meeting, and align back to the exit criteria in each meeting.
1. After a long discussion about a topic, tries to summarize it and result in an action item - Determines if this topic should be pursued further, or if it changes the exit criteria. If it does, clearly flags to the DRI and other Working Group members.
1. Creates a single source of truth for collaboration and coordination and is clear on communication expectations. Consider using an [Issue Board](https://docs.gitlab.com/ee/user/project/issue_board.html) to track working group tasks by using the ~"WorkingGroup::" scoped label on each issue. This can be done separately from any other development related issues that the working group needs to track.

**Executive Sponsor**

The E-Group member who is responsible for staying plugged into the project, supporting the Working Group DRI (if necessary), and supporting escalations (if required). This is the E-Group member most interested in the results or responsible for the outcome. It will usually be the E-Group Member who owns the function that the Working Group DRI belongs to. E-Group Sponsors should be kept in the loop--especially when there are changes to timeline/scope, interdependencies that require alignment, or input needed. They will usually attend Working Group Meetings.

The E-Group Sponsor is responsible for helping the Working Group DRI to obtain funding needed for Working Group success. This is important as the E-Group is the level at which budget dollars are owned.

The E-Group sponsor also plays a key role in being a bridge to the rest of E-Group. This person should help to flag when related topics or updates should be brought to E-Group as an update or ask.

**Functional Lead**

One or more people who represent their entire function to the working group, regularly monitors the Working Group Slack Channel, creates issues for action items, serves as DRI for issues created, actively participates in meetings, volunteers for opportunities to further the Working Groups goals, regularly attends meetings either synchronously or asynchronously, shares information learned from the Working Group with their Functional teams, volunteers to take on action items , gathers feedback from Functional teams and brings that feedback back to the Working Group.

### Optional Roles

**Member**

Any subject matter expert, attends meetings synchronously or asynchronously on a regular basis, regularly monitors the Working Group Slack Channel, shares information learned from the Working Group with their peers, and gathers feedback from their peers and brings that feedback back to the Working Group. A member may serve as a DRI for a specific workstream or activity.

## Guidelines

1. An executive sponsor is required, in part, to prevent proliferation of working groups
1. A person should not facilitate more than one concurrent working group
1. Participation in some Working Groups requires a significant time commitment. Participants should be clear on their role and expectations for what they are expected to deliver. They should manage their time and capacity and quickly escalate if they feel unable to serve in or deliver in their role
1. It is highly recommended that anyone in the working group with [OKRs](/company/okrs/) aligns them to the effort


## Process

1. Preparation
   1. Create a working group page
   1. Assemble a team from required functions
      - Share in appropriate Slack channel(s) to encourage a diverse group of participants
   1. Create an agenda doc public to the company
   1. Create a Slack channel (with `#wg_` prefix) that is public to the company
   1. Schedule a recurring Zoom meeting
1. Define a goal and exit criteria
   1. Clearly outline risks and interdependencies. Flag mitigations and ensure that interdependencies are know and being addressed as part of the plan
   1. Ensure that responsibilities for all participants are clearly documented and understood
1. Gather metrics that will tell you when the goal is met
1. Organize activities that should provide incremental progress
1. Ship iterations and track the metrics
1. Communicate the results
   1. Consider regular updates to the [#whats-happening-at-gitlab](https://gitlab.slack.com/archives/C0259241C)  slack channel as progress is made towards goal.
   1. Communicate outcomes using [multi modal communication](/handbook/communication/#multimodal-communication).
   1. Notify widely of exit outcomes via channels such as the [engineering week in review](/handbook/engineering/#communication).
1. Disband the working group
   1. Celebrate. Being able to close a working group is a thing to be celebrated!
   1. Move the working group to the "Past Working Groups" section on this page
   1. Update the working group's page with the close date and any relevant artifacts for prosperity
   1. Archive the slack channel
   1. Delete the recurring calendar meeting

### Modifications to Process for Limited Access Communications

We make things public by default because [transparency is one of our values](/handbook/values/#transparency).
Some things can't be made public and are either [internal](#internal) to the company or have [limited access](#limited-access) even within the company.
If something isn't on our [Not Public list](/handbook/communication/confidentiality-levels/#not-public), we should make it available externally. If a working group is working on something on the Not Public List, working group team members should take precautions to limit access to information until it is determined that information can be shared more broadly. To highlight a few modifications to the process above:

1. Preparation
   1. Determine an appropriate project name using [limited access naming conventions](/handbook/communication/confidentiality-levels/#limited-access).
   1. Create an overview page and add the link to [Active Working Groups](/company/team/structure/working-groups/#active-working-groups-alphabetic-order). You can share limited information, but capture key team members, including the facilitator, executive stakeholder, and functional lead.
   1. If working in the handbook, evaluate whether the page should be confidential or be housed in a new project with limited access. Consider working in the [staging handbook](/handbook/handbook-usage/#the-staging-handbook). We use this when information may need to be iterated on or MR branches may need to be created in staging before it is made public. Outside of E-Group, temporary access may be granted on a project-specific basis.
   1. Maintain a list of working group members and other folks who are participating in or informed of the project. This list should be available to all participating team members. Folks should not be added to this list until it is confirmed that they understand what can be communicated.
   1. Ensure that each working group team member understands what can be communicated externally and internally.
   1. Have private Slack channels that include folks who are directly working on the project.
   1. Limit the agenda to a specific set of folks.
1. Communicate the results
   - Communicate results and progress to the direct working group or other key stakeholders to ensure that folks are aligned and have context on key happenings. Do not share sensitive information outside of private channels.
1. Proactively share information if the project is no longer limited access
   1. Notify widely of progress or exit outcomes when information can be shared more broadly.
   1. Evaluate which artifacts and communication material can be made internally available or public.
      1. If you were working in the [staging handbook](/handbook/handbook-usage/#the-staging-handbook), follow instructions to make a merge request against the public repo.
      1. Transition members to public Slack channels and archive private channels.
      1. Deprecate private agendas. Link this to a new agenda document.
      1. Consider making GitLab Groups and Projects public or avialable to a broader audience.

## Participating in a Working Group

If you are interested in participating in an active working group, it is generally recommended that you first communicate with your manager and the facilitator and/or lead of the working group. After that, you can add yourself to the working group member list by creating a MR against the specific working group handbook page.

## Active Working Groups (alphabetic order)

1. [Account Escalation Process](/company/team/structure/working-groups/account-escalation-process)
1. [API Vision](/company/team/structure/working-groups/api-vision/)
1. [Category Leadership](/company/team/structure/working-groups/category-leadership/)
1. [ClickHouse Datastore](/company/team/structure/working-groups/clickhouse-datastore/)
1. [Cross Functional Prioritization](/company/team/structure/working-groups/cross-functional-prioritization/)
1. [Dashboards](/company/team/structure/working-groups/dashboards/)
1. [Demo & Test Data](/company/team/structure/working-groups/demo-test-data/)
1. [Engineering Internship](/company/team/structure/working-groups/engineering-internship/)
1. [Event Stream](/company/team/structure/working-groups/event-stream/)
1. [Expense Management](/company/team/structure/working-groups/expense-management/)
1. [FedRAMP Execution](/company/team/structure/working-groups/fedramp-execution/)
1. [Frontend Observability](/company/team/structure/working-groups/frontend-observability/)
1. [GitLab.com Disaster Recovery](/company/team/structure/working-groups/disaster-recovery/)
1. [GitLab.com SAAS Data Pipeline](/company/team/structure/working-groups/gitlab-com-saas-data-pipeline)
1. [Revenue Globalization](/company/team/structure/working-groups/globalization/)
1. [Issue Prioritization Framework](/company/team/structure/working-groups/issue-prioritization-framework/)
1. [Leading Organizations](/company/team/structure/working-groups/leading-organizations/)
1. [Learning Experience](/company/team/structure/working-groups/learning-experience/)
1. [Maintainership](/company/team/structure/working-groups/maintainership/)
1. [Modern Applications Go-To-Market](/company/team/structure/working-groups/modern-applications-gtm/)
1. [Multi-Large](/company/team/structure/working-groups/multi-large/)
1. [Next Architecture Workflow](/company/team/structure/working-groups/next-architecture-workflow/)
1. [Product Career Development Framework](/company/team/structure/working-groups/product-career-development-framework/)
1. [Talent Acquisition SSOT](/company/team/structure/working-groups/recruiting-ssot/)
1. [Token Management](/company/team/structure/working-groups/token-management/)


## Past Working Groups (alphabetic order)

1. [Architecture Kickoff](/company/team/structure/working-groups/architecture-kickoff/)
1. [China Service](/company/team/structure/working-groups/china-service/)
1. [CI Queue Time Stabilization](/company/team/structure/working-groups/ci-queue-stability/)
1. [Commercial & Licensing](/company/team/structure/working-groups/commercial-licensing/)
1. [Continuous Scanning](/company/team/structure/working-groups/continuous-scanning/)
1. [Contributor Growth](/company/team/structure/working-groups/contributor-growth)
1. [Database Scalability](/company/team/structure/working-groups/database-scalability/)
1. [Development Metrics](/company/team/structure/working-groups/development-metrics/)
1. [Dogfood Plan](/company/team/structure/working-groups/dogfood-plan/)
1. [Ecommerce Motion](/company/team/structure/working-groups/ecommerce)
1. [Engineering Career Matrices](/company/team/structure/working-groups/engineering-career-matrices/)
1. [Enterprise Market Leadership](/company/team/structure/working-groups/enterprise-market-leadership)
1. [Experimentation](/company/team/structure/working-groups/experimentation/)
1. [Githost Migration](/company/team/structure/working-groups/githost-migration/)
1. [GitLab.com Cost](/company/team/structure/working-groups/gitlab-com-cost/)
1. [GitLab.com Revenue](/company/team/structure/working-groups/gitlab-com-revenue/)
1. [gitlab-ui (CSS and Components)](/company/team/structure/working-groups/gitlab-ui/)
1. [GTM Product Analytics](/company/team/structure/working-groups/product-analytics-gtm/)
1. [IACV - Delta ARR](/company/team/structure/working-groups/iacv-delta-arr/)
1. [IC Gearing](/company/team/structure/working-groups/ic-gearing/)
1. [Improve Ops Quality](/company/team/structure/working-groups/improve-ops-quality/)
1. [Internal Feature Flag usage](/company/team/structure/working-groups/feature-flag-usage/)
1. [Isolation](/company/team/structure/working-groups/isolation/)
1. [Learning Restructure](/company/team/structure/working-groups/learning-restructure)
1. [Licensing and Transactions Improvements](/company/team/structure/working-groups/licensing-transactions-improvements/)
1. [Log Aggregation](/company/team/structure/working-groups/log-aggregation/)
1. [Logging](/company/team/structure/working-groups/logging/)
1. [Merge Request Report Widgets](/company/team/structure/working-groups/merge-request-report-widgets/)
1. [Minorities in Tech (MIT) Mentoring Program](/company/team/structure/working-groups/mit-mentoring/)
1. [MLOps](/company/team/structure/working-groups/mlops/)
1. [Object Storage](/company/team/structure/working-groups/object-storage/)
1. [Pipeline Validation Service Operations](/company/team/structure/working-groups/pipeline-validation-service-operations/)
1. [Performance Indicators](/company/team/structure/working-groups/performance-indicators/)
1. [Product Analytics](/company/team/structure/working-groups/product-analytics/)
1. [Product Development Flow](/company/team/structure/working-groups/product-development-flow/)
1. [Product Engagement Actions (FY21)](/company/team/structure/working-groups/FY21-product-engagement-actions/)
1. [Purchasing Reliability](/company/team/structure/working-groups/purchasing-reliability/)
1. [Rate Limit Architecture](/company/team/structure/working-groups/rate-limit-architecture/)
1. [Real-Time](/company/team/structure/working-groups/real-time/)
1. [Secure Offline Environment Working Group](/company/team/structure/working-groups/secure-offline-environment/)
1. [Self-Managed Scalability](/company/team/structure/working-groups/self-managed-scalability/)
1. [Sharding](/company/team/structure/working-groups/sharding/)
1. [Simplify Groups & Projects](/company/team/structure/working-groups/simplify-groups-and-projects/)
1. [Single Codebase](/company/team/structure/working-groups/single-codebase/)
1. [SOX PMO](/company/team/structure/working-groups/sox/)
1. [Tiering](/company/team/structure/working-groups/tiering/)
1. [Transient Bugs](/company/team/structure/working-groups/transient-bugs/)
1. [Upstream Diversity](/company/team/structure/working-groups/upstream-diversity/)
1. [Webpack (Frontend build tooling)](/company/team/structure/working-groups/webpack)

## Top Cross-Functional Initiatives

Please see the handbook page on [Top Cross-Functional Initiatives](/company/top-cross-functional-initiatives/)
