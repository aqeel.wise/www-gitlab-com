---
layout: markdown_page
title: "Category Direction - Fulfillment Admin Tooling"
description: "The Fulfillment Admin Tooling strategy page belongs to the Fulfillment Admin Tooling group of the Fulfillment stage"
---

- TOC
{:toc}

## Overview

The Admin Tooling group exists to provide internal teams with tooling and automation that allow them to provide support to our customers. Our group strives to build tools that are intuitive, easy to use, and optimise team workflows.

When our teams lack the appropriate tools to solve customer problems it leads to massive inefficiencies and, in some scenarios, leads to problems such as customers temporarily losing access to critical paid plan functionality.

Inefficiencies can present in the following ways:

- It is hard to find the customer and account information because: our internal platform lacks functionality and is not intuitive, the information is scattered across multiple systems, and the information is inconsistent across systems which creates low trust in the accuracy of the information for some systems.
- Manual intervention leads to data integrity issues, which perpetuates more manual intervention and leads to errors and unexpected states for customer accounts and subscriptions.
- Customer accounts and subscriptions in unexpected states create complexity that is harder to diagnose and troubleshoot, resulting in more time spent and more people involved to resolve the issue.
- When a bug remains unresolved or a feature is missing, it creates the need for a workaround to serve the customer. Workarounds create new workflows that the team supports and add to the ticket volume.
- Some tasks require a lengthy workflow which includes: verification of requestor and request, finding and validating information, seeking and confirming approvals from the correct people, executing the task, communicating and confirming task completion and verifying changes. 

The Fulfillment Admin Tooling team works with internal teams to build robust systems that enable our internal, customer-facing teams better support our customers. 

Key responsibilities
- Focuses on building admin tools for the support team.
- Create transparency for internal teams into customer subscription, billing and licensing.

## Mission

Provide internal teams at GitLab with the tooling they need to serve customers efficiently and in a way that minimizes repeat issues.

## Vision

Our internal teams have access to relevant customer and account information, they are able to effectively diagnose and troubleshoot issues and errors, and they are able to efficiently complete tasks and resolve issues and errors with the appropriate tooling and automation.

### Target Audience and Experience

Our audience are GitLab internal team members, with a focus on team members that help support our commercial operations. In particular: 
- [GitLab L&R Support team](https://about.gitlab.com/handbook/support/)
- [GitLab Sales team](https://about.gitlab.com/handbook/sales/)
- [GitLab Billing](https://about.gitlab.com/handbook/finance/accounting/finance-ops/billing-ops/) and Accounts Receivable team

We work in collaboration with the Support Operations and Field Operations teams to achieve internal team efficiency goals.

## Challenges to address

### Visibility and data integrity

Our internal teams don't always have the visibility they need to quickly serve a customer. The longer it takes to find information, troubleshoot, and then resolve an issue, the more likely customer dissatisfaction becomes. Our internal systems should provide our teams with access to all the relevant information they need (preferably in one location) to resolve problems or complete a request. 

Where there are long-standing bugs or missing customer-facing features, the Support team has built the Mechaniser tool as a temporary work-around to address required functionality for troubleshooting and resolving customer issues and tasks. Since Mechaniser is not built or maintained by a GitLab engineering team and it leads to data integrity issues, it is important that this tool is deprecated in favour of intuitive tooling and automation that can provide needed functionality.

**Projects to tackle this challenge:**

- Migrate Mechanizer features so it can be removed
- Solve for trial plan change
- Solve for clear subscription
- Implement minutes and storage change ability in CustomersDot
- Fix individual integrity issues for gitlab.com <> CustomersDot subscriptions 
- CustomersDot Admin maintenance
- Auditing

### Efficiency

What does an efficient L&R Support team look like?

- Easy to find the information needed to troubleshoot a problem.
- Minimal involvement required from other teams to troubleshoot a problem or complete a task.
- Able to access information quickly and ideally from a single source.

What does an efficient Sales team look like?

- Able to easily and quickly provide customer with access to GitLab product as needed to support the sales process.
- Approvals processes are intuitive and do not hamper the sales process.
- Has visibility into relevant customer information to support sales process or to proactively or reactively resolve customer issues.

**How we will tackle this:**

- Review inefficient workflows and identify opportunities for tooling and automation to cut down on time spent searching for relevant information and completing tasks.
- Identify workflows where multiple teams are involved to complete a task and explore solutions to bring ownership and supportive tooling to the relevant team.
- Review inefficiencies in approvals processes and explore opportunities for tooling or automation to improve efficiency.

## KPIs

- Volume of manual subscription modifications by support.
- Volume of internal requests from Sales to L&R Support.
- Volume of escalations measured by L&R Support Ticket Attention Requests (STAR).
- Ratio of L&R ticket volume vs GitLab subscriptions.

## 1 Year Plan

Over the next 12 months, the Admin Tooling team has the following primary objectives:

1. Deprecate the use of Mechaniser and build the needed functionality into our platform (customers.gitlab.com).
1. Implement audit trail for actions performed where customer accounts/namespaces/subscriptions are modified for the purpose of supporting compliance and measuring volume of modifications.
1. Create, update and maintain admin tooling functionality on our platform and ensure that features keep data integrity, with clear errors and activity logging.

For a list of upcoming and ongoing projects, see our [GitLab epic roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=start_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name%5B%5D=Fulfillment+Roadmap&label_name%5B%5D=group%3A%3Aadmin+tooling&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP).
