---
layout: markdown_page
title: "Product Direction - Release"
description: "This is the GitLab direction page for the Release stage. The Release stage is the point at which an application is ready for deployment."
canonical_path: "/direction/release/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product direction for the Release stage. This direction is aligned with the broader [Delivery Direction](/direction/delivery).

### Mission
Help our customers deploy software to production.

### Vision
Our vision is to enable organizations and teams to perform all software deployment activities easily, confidently, and safely. GitLab helps teams collaborate on what needs to be done to ensure software is ready to be shipped to production. GitLab aims to automate as much of these activities as possible leveraging existing data and integrating with other DevOps stages in the platform.

### Letter from the Editor - FY23
To the GitLab Community and customers,

In the next year, we will be executing on our exciting deployment vision to enable organizations to make deployments easy, flexible, and secure. We have recently shipped a new design for the Environments page to make it easier to understand and see pertinent information related to your deployments. We have also shipped our MVC for deployment approvals to allow organizations to garner the necessary approvals and meet compliance requirements for production deployments. We have continued iterations of the deployment approval feature to view approval decision and comment information in the UI. We also shipped an MVC for search in the Environments page and are currently iterating to make it better. Next we will be enhancing our Environments capabilities with group-level functionality to make this feature even more useful and delightful. Next, we will be continuing to mature our Release Orchestration capabilities to ensure release teams have the ability to coordinate complex releases across multiple projects and automate these activities as much as possible. Features include a streamlined way to create CHANGLOG messages, an auto-populating single view of related production environments for an application, and generating release artifacts for audit and compliance. Furthermore, we will also be looking at ways to even further integrate with our world-class Create and Verify capabilities so that our users can take advantage of these powerful deployment features.

I have also been thinking about how to jumpstart our internal dogfooding of more of the Release capabilities. This will no doubt have an impact on increasing feedback loops and understanding where we need to make improvements for our categories. Lastly, our community has always been an excellent source of feedback and contributions. Furthering our efforts to engage you all will pay off as we continue to bring our vision to life.

### Strategy
Our mid-term product strategy is defined below by key principles and a prioritized set of opportunities to pursue.

Key principles:
- Deployment as a first-class concept
- Differentiate by connections to other features
- Support pull-based and push-based deployment strategies

Opportunity: Deployment visibility and quick actions to deploy quickly for technical teams

Many teams are deploying using CI pipelines or even GitOps with GitLab and sometimes, other tools today. However, these activities are not well represented in the UI. A simple example - a team may have a deployment pipeline defined in their .gitlab-ci.yml file that consists of jobs that execute scripts related to deploying a version of code to a production environment. Since these deployment pipelines are buried within the other pipeline definitions, they are difficult to track. We assert that deployments are special types of pipelines that warrant their own view and treatment. In other words, we want to make deployments and other related deployment-time concepts like services first class citizens in GitLab.

Teams need to be able have a historical view of deployments. They need to see the status of an ongoing deployment. They need to understand the health and status of production and other environments. They need to have structure and standards around their deployment processes to meet compliance and auditing needs. They need to quickly and [easily promote code through environments](https://gitlab.com/groups/gitlab-org/-/epics/9088) all the way to production. They need to rollback problematic deployments. There is opportunity to support these types of activities within the UI for both push-based and pull-based deployments. All of these activities are core to releasing new features to production and apply to virtually all teams doing software development. Furthermore, because deployment occurs after upstream activities that are likely being done GitLab already, there is ease of integration with other features in the product and helps differentiate deployment presentment and execution from competitors that don't have all of this activity in one system.

We have made significant progress with the above with the environments page redesign, deploy approval, and utilities to help manage environments. We are now starting to focus on helping users deploy and understand their multi-project application deployments. This builds on the existing foundation laid out above. Examples of these types of problems include [tracking deployments across multiple projects](https://gitlab.com/groups/gitlab-org/-/epics/4276), coordinating deployments between teams, and understanding what is being deployed at a given time by teams.

Opportunity: [Deploy approval](https://gitlab.com/groups/gitlab-org/-/epics/6832) support

Certain customer segments in highly regulated industries such as government or banking, require ways to ensure and audit that deployments to special environments to production are approved by the necessary parties for compliance.

For the past few months, we have shipped iterations to the Deploy Approval feature have seen growth in the usage of this feature and a corresponding growth in overall adoption of deployments. This feature is strategic since it potentially unlocks to new customers to GitLab that require this compliance before they adopt it for deploying. Additionally, it is a key feature to making the product a source of truth for auditing, and thus more valuable, and has the potential to bring in net new users to GitLab, such as Release Managers, tech leads, QA, or others that may need to be involved with approving deployments.

Opportunity: [Onboarding](https://gitlab.com/groups/gitlab-org/-/epics/6821)
Today, x% of customers use the Release stage. There is opportunity for increasing this figure significantly by increasing awareness of environments, deployments, and releases in GitLab during key upstream touchpoints in the user journey of adopting GitLab.

Opportunity: Release orchestration and processes automation
After developing tools to visualize and trigger deployments that are mostly driven by pipelines or IaC, we will turn our attention to helping organizations solve the release orchestration process, giving them tools to plan and execute releases from the UI.

Opportunity: Advanced Deployment strategies to deploy even faster
After solving the release execution process, we will build more advanced deployment capabilities for users to allow them to smartly, safely, and quickly deploy features.

Opportunity: Deployment setup with UI

### Target personas
We build solutions targeting the following personas:
- [Priyanka (Platform Engineer)](/handbook/product/personas/#priyanka-platform-engineer)
- [Rachel (Release Manager)](/handbook/product/personas/#rachel-release-manager)
- [Delaney (Development Team Lead)](/handbook/product/personas/#delaney-development-team-lead)

### Performance Indicators (PIs)
The [Key Performance Indicator](/handbook/ceo/kpis/) for the Release stage is the [Release SMAU](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/ops-section/#releaserelease---smau---count-of-users-triggering-deployments-using-gitlab) (internal).

### Categories
- [Continuous Delivery](/direction/release/continuous_delivery/)
- [Advanced Deployments](/direction/release/advanced_deployments/)
- [Feature Flags](/direction/release/feature_flags/)
- [Release Orchestration](/direction/release/release_orchestration/)
- [Release Evidence](/direction/release/release_evidence/)
- [Environment Management](/direction/release/environment_management/)

### What's next and why

We are currently focusing on improving the experience for [Environments](https://docs.gitlab.com/ee/ci/environments/). We believe that environments is the centralized place for users to think about and manage their deployments, and currently it is the main link between CI pipelines and representing deployments in environments in the product. We want to make the Environment and related the deployment workflow user experience as useful as possible to encourage more adoption of the feature from existing customers as well as ensure prospect customers have the robust deployment safety and scalable tools they require when they purchase the product.

One important feature that we have seen rapid adoption of and will continue iterating on to support more use cases is [Deployment Approval](https://docs.gitlab.com/ee/ci/environments/deployment_approvals.html). Please see [what's next](https://about.gitlab.com/direction/release/environment_management/#whats-next) for environment management for more details about the category. We are also scoping opportunities to link our existing features to each other, such as Environments and Releases, as well as linking them to other stage functionality, e.g. Releases and Packages. One key feature we are scoping is to [Add Environments support to the GitLab Agent](https://gitlab.com/gitlab-org/gitlab/-/issues/352186) so that users can easily see what is running in or being deployed to a cluster.

After further progress on environment management and cross-stage features, we will return our focus on [Release Orchestration](/direction/release/release_orchestration/#whats-next--why) to enable our customers to coordinate complex and multi-project releases. Furthermore, we will continue to add more instrumentation so we have a better understanding of how existing users are leveraging the Release stage capabilities and what opportunities we have to increase adoption of key features such as Environments.

Many of the Release Stage categories such as Feature Flags and Advanced Deployments are not prioritized at the moment, though they are very important to the stage. We want to continue and push forward our leadership in the CI/CD category and thus are heavily focusing on Environment Management and Release Orchestration since they directly support this goal.

#### FY23-Q4 OKRs

[Q4 OKRs for the Release Stage](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/195) (internal)

#### FY23-Q4 Plan

| Project | What | Why |
| ------ | ---- | --- |
| [Deployment approval iterations](https://gitlab.com/groups/gitlab-org/-/epics/6832) | Continue iterations for the feature including notifications, possibly Deployment page, more complex use cases | Increase feature usage and adoption. This feature enables a key workflow that help customers adopt GitLab as a whole and/or upgrade to ~"GitLab Premium". ~"deployment-direction"  |
| [Group Environments](https://gitlab.com/groups/gitlab-org/-/epics/7558) | Ship iterations for group environments. | Highly requested feature for large organizations to understand their application and production environment. Impact to Environments MAU. ~"deployment-direction" |
| [Kubernetes Agent and Environments MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/352186) | Partner with ~"group::configure" and design and ship a first iteration for integrating Environments and the K8s Agent  | Building this will increase Environments page MAU and help increase K8s Agent adoption (one of the OKRs for ~"group::configure") and also supports Ops CMAU goal. ~"deployment-direction" |
| Onboarding experiment | Ship an experiment that encourages users to create a deployment job to a `production` environment | This has a potential for outsized impact to SMAU growth rate since our SMAU is currently based on the number of users who deploy to a GitLab environment. |
| [Environments Cleanup](https://gitlab.com/groups/gitlab-org/-/epics/5920)| Help users manage large numbers of environments. Ship a way for users to cleanup stale environments.| Highly requested feature. Larger number of environments are currently difficult to manage and navigate. Will impact usability/efficiency and MAU of the page|
| [Promotion to Production workflow](https://gitlab.com/groups/gitlab-org/-/epics/9088) | Provide a method for users to configure and execute promotion of code from dev, to staging to production. | Key workflow that we removed recently. Users definitely want this and we want to provide an easy, but also robust way for them to configure and execute this common workflow. |
| [Usability benchmark improvements](https://gitlab.com/groups/gitlab-org/-/epics/9287) | Ship improvements to usability as recommended from the usability benchmark study conducted in Q3 | Help improve usability of the product |
| [Environment search performance](https://gitlab.com/groups/gitlab-org/-/epics/8467) | Ship follow up improvements to make sure the search feature is performant on SaaS | This is critical for ensuring overall .com reliability. Improvements include instrumentation, and design changes to support performant queries |
| [Error budget improvements](https://gitlab.com/groups/gitlab-org/-/epics/9149) | Ship improvements to help with error budget | Improve availability of Release Stage features |

source (internal): https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/196

If you are interested specifically in what we are working on next, please see our plans for the [15.7 milestone](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/174). For a medium term view of our plans, please see our [roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=opened&sort=start_date_asc&layout=WEEKS&label_name%5B%5D=devops%3A%3Arelease&label_name%5B%5D=Roadmap&progress=WEIGHT&show_progress=true&show_milestones=false&milestones_type=ALL). If you have questions or feedback, please reach out via [this issue](https://gitlab.com/gitlab-com/Product/-/issues/4643)!
